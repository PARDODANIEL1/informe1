close all
clear all
clc

oldobj = instrfind;
if not(isempty(oldobj))
    fclose(oldobj);
    delete(oldobj);
end 

if ~exist('s','var')
    s= serial('COM9','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end 

if ~isvalid(s)
    s= serial('COM9','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);    
end

if  strcmp(get(s,'status'),'closed')
    fopen(s);
end 
SENSITIVITY_ACCEL = 2.0/32768.0;
SENSITIVITY_GYRO = 250.0/32768.0;
offset_accelx=-7008.00;
offset_accely=1112.00;
offset_accelz=16982.00;
offset_gyrox=460.50;
offset_gyroy=-254.00;
offset_gyroz= -365.00 ;



disp('En sus marcas.Posicione el sensor en la posicion inicial')
pause();

disp('comienza')
fprintf(s,'H');
i=1;
while(1)
    str{i} = fscanf(s);
    if(str{i}(1)== 'A') 
        disp('termina')
        break;
    end 
    i= i + 1;
end
   
fclose(s);
n = length(str)-1;
c = 1;
for i=1:n
    temp = cellfun(@str2num,strsplit(str{i},','));   
    if numel(temp)== 8 
        values(i,:) = temp;
    end
end

save DatosTest1 values
%-------------------------------------------------------------------------
%Figuras
%-----------------------------------------------------------------------
Nsamples = length(values);
dt=0.01;
t=0:dt:Nsamples*dt-dt;
%------------------
%acelerometro raw
%------------------
figure;
plot(t, values(:,3)*SENSITIVITY_ACCEL, 'b')%ax
hold on
plot(t, values(:,4)*SENSITIVITY_ACCEL, 'r')%ay
plot(t, values(:,5)*SENSITIVITY_ACCEL, 'g')%az
title('Acelerometro MPU6050')
ylabel(' Aceleracion (g)')
xlabel('Tiempo(Segundos)')
legend('ax','ay','az','Location','northeast','Orientation','horizontal')
%acelerometro calibrado
figure;
plot(t, (values(:,3)-offset_accelx)*SENSITIVITY_ACCEL, 'b')%ax
hold on
plot(t, (values(:,4)-offset_accely)*SENSITIVITY_ACCEL, 'r')%ay
plot(t, (values(:,5)-(offset_accelz-(32768/2)))*SENSITIVITY_ACCEL, 'g')%az
title('Acelerometro calibrado MPU6050')
ylabel('Aceleración (g)')
xlabel('Tiempo(Segundos)')
legend('ax','ay','az','Location','northeast','Orientation','horizontal')
%gyroscopio raw
%------------------
figure;
plot(t, values(:,6)*SENSITIVITY_GYRO, 'b')%ax
hold on
plot(t, values(:,7)*SENSITIVITY_GYRO, 'r')%ay
plot(t, values(:,8)*SENSITIVITY_GYRO, 'g')%az
title('Gyroscopio MPU6050')
ylabel('velocidad Angular (°/s)')
xlabel('Tiempo(Segundos)')
legend('ax','ay','az','Location','northeast','Orientation','horizontal')
%Gyroscopio calibrado
figure;
plot(t, (values(:,6)-offset_gyrox)*SENSITIVITY_GYRO, 'b')%ax
hold on
plot(t, (values(:,7)-offset_gyroy)*SENSITIVITY_GYRO, 'r')%ay
plot(t, (values(:,8)-offset_gyroz)*SENSITIVITY_GYRO, 'g')%az
title('Gyroscopio calibrado MPU6050')
ylabel('velocidad Angular (°/s)')
xlabel('Tiempo(Segundos)')
legend('ax','ay','az','Location','northeast','Orientation','horizontal')